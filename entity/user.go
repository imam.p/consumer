package entity

type User struct {
	Name    string
	Address string
}

func ChangeUser(user User) {
	user.Name = "tes"
	user.Address = "tes ajah ini mah"
}
func ChangeUserPointer(user *User) {
	user.Name = "tes"
	user.Address = "tes ajah ini mah"
}
