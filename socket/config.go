package socket

import "github.com/gorilla/websocket"

type M map[string]interface{}

const (
	MESSAGE_NEW_USER = "new user"
	MESSAGE_CHAT     = "chat"
	MESSAGE_LEAVE    = "leave"
)

//a = *WebSocket

var Connections = make([]*WebSocketConnection, 0)

type SocketPayload struct {
	Message string
}
type SocketResponse struct {
	From    string
	Type    string
	Message string
}
type WebSocketConnection struct {
	*websocket.Conn
	Username string
}

var Wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}
