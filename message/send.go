package message

import (
	"fmt"
)

var messages = make(chan string)

var SaySomething = func(who string) {
	var data = fmt.Sprintf("hello %s", who)
	messages <- data
	fmt.Println("message nya", messages)
}
