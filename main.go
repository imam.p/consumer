package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
	"websocket/entity"
	"websocket/socket"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/jung-kurt/gofpdf"
	"github.com/novalagung/gubrak/v2"
)

func TesGoroutine() {
	fmt.Println("tes goroutine ini mah")
}

func main() {
	go TesGoroutine()
	time.Sleep(1 * time.Second)
	fmt.Println("===task goroutine dan pointer===")

	var change = entity.User{
		Name: "tes pointer",
	}

	entity.ChangeUser(change)
	fmt.Println("========= Tanpa Pointer =============")
	fmt.Println("hasil di ubah Nama :", change.Name)
	fmt.Println("hasil di ubah Address :", change.Address)

	fmt.Println("======================")
	fmt.Println("========= Dengan Pointer =============")

	entity.ChangeUserPointer(&change)
	fmt.Println("hasil di ubah Nama :", change.Name)
	fmt.Println("hasil di ubah Address :", change.Address)

	fmt.Println("======================")
	fmt.Println("======================")

	// start consumer
	// customFormatter := kafka.GetLogRus()
	// logrus.SetFormatter(customFormatter)
	// kafkaConfig := kafka.GetKafkaConfig("", "")
	// consumers, err := sarama.NewConsumer([]string{"localhost:9092"}, kafkaConfig)
	// if err != nil {
	// 	logrus.Errorf("Error create kakfa consumer got error %v", err)
	// }
	// defer func() {
	// 	if err := consumers.Close(); err != nil {
	// 		logrus.Fatal(err)
	// 		return
	// 	}
	// }()

	// kafkaConsumer := &kafka.KafkaConsumer{
	// 	Consumer: consumers,
	// }

	// signals := make(chan os.Signal, 1)
	// kafkaConsumer.Consume([]string{"test_topic"}, signals)

	// end consumer

	// router := gin.Default()
	r := gin.Default()
	r.LoadHTMLFiles("chatbox.html")
	r.Use(cors.Default())
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "chatbox.html", nil)
	})

	r.GET("/ws", func(c *gin.Context) {
		wshandlerChat(c.Writer, c.Request)
	})

	r.POST("/generate", func(c *gin.Context) {
		pdf(c.Param("data"))
	})

	r.Run("localhost:8081")
}

func wshandlerChat(w http.ResponseWriter, r *http.Request) {
	currentGorillaConn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
	if err != nil {
		http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
	}

	username := r.URL.Query().Get("username")
	currentConn := socket.WebSocketConnection{Conn: currentGorillaConn, Username: username}
	socket.Connections = append(socket.Connections, &currentConn)

	go handleIO(&currentConn, socket.Connections)
}

func handleIO(currentConn *socket.WebSocketConnection, connections []*socket.WebSocketConnection) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("ERROR", fmt.Sprintf("%v", r))
		}
	}()

	broadcastMessage(currentConn, socket.MESSAGE_NEW_USER, "")

	for {
		payload := socket.SocketPayload{}
		err := currentConn.ReadJSON(&payload)
		if err != nil {
			if strings.Contains(err.Error(), "websocket: close") {
				broadcastMessage(currentConn, socket.MESSAGE_LEAVE, "")
				ejectConnection(currentConn)
				return
			}

			log.Println("ERROR", err.Error())
			continue
		}

		broadcastMessage(currentConn, socket.MESSAGE_CHAT, payload.Message)
	}
}

func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := socket.Wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade:", err)
		return
	}

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		conn.WriteMessage(t, msg)
	}
}

func broadcastMessage(currentConn *socket.WebSocketConnection, kind, message string) {
	for _, eachConn := range socket.Connections {
		if eachConn == currentConn {
			continue
		}

		eachConn.WriteJSON(socket.SocketResponse{
			From:    currentConn.Username,
			Type:    kind,
			Message: message,
		})
	}
}
func ejectConnection(currentConn *socket.WebSocketConnection) {
	filtered := gubrak.From(socket.Connections).Reject(func(each *socket.WebSocketConnection) bool {
		return each == currentConn
	}).Result()
	socket.Connections = filtered.([]*socket.WebSocketConnection)
}

func pdf(data string) {
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.SetFont("Arial", "B", 16)
	pdf.MoveTo(0, 10)
	pdf.Cell(1, 1, "Hello world")
	err := pdf.OutputFileAndClose("hello.pdf")
	if err == nil {
		fmt.Println("PDF generated successfully")
	}
}
